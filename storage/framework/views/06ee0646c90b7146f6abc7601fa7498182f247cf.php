
<ul class="nav navbar-nav side-nav">
	<li class="<?php if(Request::segment(1)=='home'): ?><?php echo e('active'); ?><?php else: ?> <?php echo e(''); ?><?php endif; ?>">
		<a href="<?php echo e(route('home')); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
	</li>
	<li class="<?php if(Request::segment(1)=='questions' || Request::segment(1)=='add-question' || Request::segment(1)=='edit-question'): ?><?php echo e('active'); ?><?php else: ?> <?php echo e(''); ?><?php endif; ?>">
		<a href="<?php echo e(route('questionListing')); ?>"><i class="fa fa-question-circle"></i> Questions</a>
	</li>
	<li class="<?php if(Request::segment(1)=='surveys'|| Request::segment(1)=='add-survey' || Request::segment(1)=='edit-survey'): ?><?php echo e('active'); ?><?php else: ?> <?php echo e(''); ?><?php endif; ?>">
		<a href="<?php echo e(route('surveyListing')); ?>"><i class="fa fa-tasks"></i> Survey</a>
	</li>
</ul>