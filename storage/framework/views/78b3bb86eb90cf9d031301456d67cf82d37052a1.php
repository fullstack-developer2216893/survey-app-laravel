<!DOCTYPE html>

<html lang="en">



<head>



    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">



    <title><?php echo e(config('app.name', 'Laravel')); ?></title>



    <!-- Bootstrap Core CSS -->

    <link href="<?php echo e(asset('public/css/bootstrap.min.css')); ?>" rel="stylesheet">

	<link href="<?php echo e(asset('public/css/sb-admin.css')); ?>" rel="stylesheet">

	<link href="<?php echo e(asset('public/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">

	<link href="<?php echo e(asset('public/css/style.css')); ?>" rel="stylesheet">

	<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

	<!-- Fonts -->

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

</head>



<body>

	<div id="wrapper">

		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

                <a class="navbar-brand" href="index.html">Survey Admin</a>

            </div>

            <?php echo $__env->make('includes.top-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="collapse navbar-collapse navbar-ex1-collapse">

                <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            </div>

        </nav>

		<div id="page-wrapper">

		

			<?php if(Session::has('success')): ?>

				<div class="alert alert-success">

					<strong>Success!</strong> <?php echo e(Session::get('success')); ?>


				</div>

			<?php endif; ?>

			<?php if(Session::has('warning')): ?>

			<div class="alert alert-danger">

				<strong>Warning!</strong> <?php echo e(Session::get('warning')); ?>


			</div>

			<?php endif; ?>

			

			<div class="container-fluid">

				<?php echo $__env->yieldContent('content'); ?>

            </div>

            

		</div>

    </div>

    

	<!--scripts-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="<?php echo e(asset('public/js/validate.min.js')); ?>"></script>
    
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

	 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

    <script src="<?php echo e(asset('public/js/bootstrap.min.js')); ?>"></script>
    <script src="//cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>

	<?php echo $__env->yieldContent('myjsfile'); ?>

	<script src="<?php echo e(asset('public/js/common.js')); ?>"></script>

</body>



</html>

