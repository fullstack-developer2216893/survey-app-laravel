<ul class="nav navbar-right top-nav">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo e(Auth::user()->name); ?> <b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li>
				<a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
						   onclick="event.preventDefault();
										 document.getElementById('logout-form').submit();">
					<i class="fa fa-fw fa-power-off"></i> Log Out
					<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
							<?php echo csrf_field(); ?>
					</form>
				</a>
			</li>
		</ul>
	</li>
</ul>