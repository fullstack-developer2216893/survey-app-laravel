<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();

});



Route::post('auth/register', 'UserController@register');

Route::post('auth/login', 'UserController@login');

Route::post('forgot-password', 'UserController@forgotPassword');

Route::post('change-password/{uid}', 'UserController@changePassword');
Route::get('user-submissions/{uid}', 'SurveySubmissionController@userSubmissionListing');
Route::group(['middleware' => 'jwt.auth'], function () {
	Route::get('user', 'UserController@getAuthUser');
    Route::get('survey-single-detail/{uid}', 'SurveyController@surveySingleDetail');
	Route::get('send-pdf-email', 'SurveySubmissionController@sendEmail');
	Route::get('view-user-submission/{uid}/{sno}', 'SurveyController@viewUserSurvey');
    Route::resource('surveys','SurveyController');
	
    Route::post('survey-submit/{sid}/{uid}/{sno}','SurveySubmissionController@SubmitSurvey');		
	
});

Route::group(['middleware' => ['cors']], function(){ 
});