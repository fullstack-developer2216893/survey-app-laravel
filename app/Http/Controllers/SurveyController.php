<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Question;

use App\Survey;

use App\SurveyOption;

use App\QuestionOption;

use Illuminate\Support\Facades\Session;

use App\SurveySubmission;

class SurveyController extends Controller

{

	protected static $model = "App\Survey";

	public function index(Request $request) {
		$postedArr=$request->all();
		return Survey::all();

	}


	public function surveySingleDetail($uid){
		$surveyList=Survey::orderBy('created_at', 'asc')->take(1)->get();
		if($surveyList){
			$id=$surveyList[0]->id;
			$questions=Survey::with(['questions.question.options'])->find($id)->toArray();
			$totalSubmission=$this->userSubmissionCount($uid);
			if($questions['questions']){

				foreach($questions['questions'] as $key=>$question){

					//echo "<pre>";print_r($question);

					if($question['question']['type']=='mc'){

						foreach($question['question']['options'] as $k=>$opt){

							$questions['questions'][$key]['question']['options'][$k]['checked']=false;

						}

					}

				}

			}
			$questions['submission_no']=$totalSubmission+1;
			
			return $questions;
		}else{
			return false;
		}
		
	}

	public function userSubmissionCount($uid){

		if($uid){
			$surveyList=SurveySubmission::with(['survey'])->where('user_id',$uid)->groupBy('submission_no')->get()->toArray();
			if($surveyList){
				$length=count($surveyList);
				return $surveyList[$length-1]['submission_no'];
			}else{
				return 0;
			}
		}

	}

	public function show($id){

		$questions=Survey::with(['questions.question.options'])->find($id)->toArray();

		if($questions['questions']){

			foreach($questions['questions'] as $key=>$question){

				//echo "<pre>";print_r($question);

				if($question['question']['type']=='mc'){

					foreach($question['question']['options'] as $k=>$opt){

						$questions['questions'][$key]['question']['options'][$k]['checked']=false;

					}

				}

			}

		}

		return $questions;

	}

	

	public function viewUserSurvey($uid,$sno){

		$submittedSurveys=\DB::table('survey_submissions')

            ->join('surveys', 'surveys.id', '=', 'survey_submissions.survey_id')

            ->join('questions', 'questions.id', '=', 'survey_submissions.question_id')

            ->select('surveys.name','surveys.created_at','surveys.updated_at','questions.*','survey_submissions.question_id','survey_submissions.user_id','survey_submissions.survey_id','survey_submissions.answer')

			->where(array('survey_submissions.user_id'=>$uid,'survey_submissions.submission_no'=>$sno))

			->groupBy('survey_submissions.question_id')

            ->get()->toArray();

		$finalArray=$submittedSurveys;
		//echo "<pre>";print_r($finalArray);die;
		

		if($finalArray){

			$i=0;

			foreach($submittedSurveys as $question){

				$finalArray[$i]->options=[];

				

				if($question->type=='t' || $question->type=='cq'){

					$finalArray[$i]->options[$question->id]['answer']=$question->answer;

				}else{

					$options=QuestionOption::where('question_id',$question->id)->get()->toArray();

					

					$answerArr=SurveySubmission::where(array('question_id'=>$options[0]['question_id'],'user_id'=>$uid,'survey_submissions.submission_no'=>$sno))->get()->toArray();

					if($options){
						$j=0;
						foreach($options as $opt){
							$finalArray[$i]->options[$j]=$opt;

							$finalArray[$i]->options[$j]['answer']=(isset($answerArr[$j]))?$answerArr[$j]['answer']:$answerArr[0]['answer'];
							if($question->type=='sc'){
								if(!$finalArray[$i]->options[$j]['answer']){
									$finalArray[$i]->options[$j]['answer']=$options[0]['value'];
								}
							}
							$finalArray[$i]->options[$j]['checked']=($finalArray[$i]->options[$j]['answer']=='1')?true:false;
							$j++;

						}

					}

				}

				$i++;

			}

		}

		return $finalArray;

	}

	

	function listing(){

		$allSurveys=Survey::all();

		return view('survey.index')->with(compact('allSurveys'));

	}

	

    function addSurvey(Request $request){

		$survey=new Survey;

		if ($request->isMethod('post')) {

			$postedArr=$request->all();

			$survey->name=$postedArr['name'];

			$survey->save();
			if(isset($postedArr['hiddenQuestionIds']) && $survey->id>0){

				$hiddenQuestionIds=explode(",",$postedArr['hiddenQuestionIds']);
				$hiddenQuestionIds=array_filter($hiddenQuestionIds);
				foreach($hiddenQuestionIds as $qid){

					$SurveyOption=new SurveyOption;

					$SurveyOption->question_id=$qid;

					$SurveyOption->survey_id=$survey->id;

					$SurveyOption->save();

				}

			}

			Session::flash('success', 'Survey added successfully.');

			return redirect()->route('surveyListing');

		}

		$questionData=Question::all();

    	return view('survey.add')->with(compact('questionData'));

    }

	

	function editSurvey($id,Request $request){

		$surveyQuestions=SurveyOption::with(['question'])->where('survey_id',$id)->get();

		//echo "<pre>";print_r($surveyQuestions[0]->question);die;

		$survey=Survey::find($id);

		if ($request->isMethod('post')) {

			$postedArr=$request->all();

			$survey->name=$postedArr['name'];

			$survey->save();

			if(isset($postedArr['hiddenQuestionIds'])){

				SurveyOption::where('survey_id',$id)->delete();
				$hiddenQuestionIds=explode(",",$postedArr['hiddenQuestionIds']);
				$hiddenQuestionIds=array_filter($hiddenQuestionIds);
				
				foreach($hiddenQuestionIds as $qid){

					$SurveyOption=new SurveyOption;

					$SurveyOption->question_id=$qid;

					$SurveyOption->survey_id=$id;

					$SurveyOption->save();

				}

			}

			Session::flash('success', 'Survey updated successfully.');

			return redirect()->route('surveyListing');

		}

		$questionData = \DB::table('questions')

            ->leftjoin('survey_options', 'questions.id', '=', 'survey_options.question_id')

			->whereNull('survey_options.question_id')

			->select('questions.*')

            ->get();

		

    	return view('survey.edit')->with(compact('survey','questionData','surveyQuestions'));

    }

	

	function deleteSurvey($id){

		if($id){

			Survey::find($id)->delete();

			Session::flash('success', 'Survey deleted successfully.');

			return redirect()->route('surveyListing');

		}

	}

}

