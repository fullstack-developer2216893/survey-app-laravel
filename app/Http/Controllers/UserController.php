<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;



use App\Http\Requests;

use App\Http\Controllers\Controller;

use JWTAuth;

use App\User;

use JWTAuthException;

use Mail;

use Illuminate\Support\Facades\Hash;

class UserController extends Controller

{   



    private $user;

    public function __construct(User $user){

        $this->user = $user;

    }

   

    public function register(Request $request){

    	if($request->all()){

    		$user = User::where('email', '=', $request->get('email'))->first();

    		if($user!=null && $user->count()>0){

    			return response()->json(['status'=>false,'message'=>'email already exist']);

    		}

    		if($request->get('name') && $request->get('email') && $request->get('password')){

    			$user = $this->user->create([

	          	'name' => $request->get('name'),

	          	'email' => $request->get('email'),

	          	'password' => bcrypt($request->get('password'))

	        	]);

    		}else{

    			return response()->json(['status'=>false,'message'=>'required fields can not empty']);

    		}

    		

        	return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);

    	}else{

    		return response()->json(['status'=>false,'message'=>'Please enter required fields']);

    	}

        

    }

    

    public function login(Request $request){

		$postedArr=$request->all();

        $credentials = $request->only('email', 'password');

		$userDetail=User::where('email',$postedArr['email'])->first()->toArray();

        $token = null;

        try {

           if (!$token = JWTAuth::attempt($credentials)) {

            return response()->json(['invalid_email_or_password'], 422);

           }

        } catch (JWTAuthException $e) {

            return response()->json(['failed_to_create_token'], 500);

        }

		$userId=$userDetail['id'];

        return response()->json(compact('token','userId'));

    }



    public function getAuthUser(Request $request){

        $user = JWTAuth::toUser($request->token);

        return response()->json(['result' => $user]);

    }



    private function randomPassword() {

        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';

        $pass = array();

        $alphaLength = strlen($alphabet) - 1;

        for ($i = 0; $i < 10; $i++) {

            $n = rand(0, $alphaLength);

            $pass[] = $alphabet[$n];

        }

        return implode($pass);

    }



    public function forgotPassword(Request $request){

        $postedArr=$request->all();

        if($postedArr['email']){

            $user=User::where('email',$postedArr['email'])->first()->toArray();
            if($user){
            	$newPassword=$this->randomPassword();

            	$newPasswordString=bcrypt($newPassword);

				$user['newPassword']=$newPassword;

           	 	Mail::send('emails.forgotPassword',$user, function($message) use ($user) {

					$message->from('hello@app.com', 'Survey Admin');

                	$message->to($user['email']);

                	$message->subject('Survey App Password');

            	});

				\DB::table('users')

            	->where('email', $postedArr['email'])

            	->update(['password' => $newPasswordString]);

            	return response()->json(['status'=>true,'message'=>'Please check your email for new password']);

            }else{
            	return response()->json(['status'=>false,'message'=>'Email not exist in database']);
            }
            
        }

    }

	

	public function changePassword($uid,Request $request){

		$postedArr=$request->all();

		extract($postedArr);

		$userDetail=User::find($uid);

		if(!Hash::check($password, $userDetail->password)){

			return response()->json(['status'=>false,'message'=>'Old password does not match the database password.']);

		}

		if($uid){

			

			$newPassword=bcrypt($newPassword);

			$userDetail->password=$newPassword;

			$userDetail->save();

			return response()->json(['status'=>true,'message'=>'Password changed successfully']);

		}

		return response()->json(['status'=>false,'message'=>'There is some problem in change password.']);

	}

}  



