@extends('layouts.app-after-login')



@section('content')

<div class="row">

	<div class="col-lg-12">

		<h1 class="page-header">

			Question Listing

		</h1>

		<a href="{{route('questionAdd')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> Add</a>

		<ol class="breadcrumb">

			<li class="active">

				<i class="fa fa-dashboard"></i> Questions

			</li>

			

		</ol>

	</div>

</div>

    <div class="row justify-content-center">

        <div class="col-md-12">

			 <table id="myTable" class="table table-striped table-bordered" style="width:100%">

        <thead>

            <tr>

                <th>Id</th>

                <th>Title</th>

                <th>Type</th>

                <th>Created</th>

                <th>Action</th>

            </tr>

        </thead>

        <tbody>

        	@if($allQuestions)

        		@foreach($allQuestions as $qt)

            <tr>

                <td>{{$qt->id}}</td>

                <td>{{$qt->title}}</td>

                <td>

                	@switch($qt->type)

					    @case('t')

					        <span>Text</span>

					        @break



					    @case('mc')

					        <span>Multiple Choice</span>

					        @break

						@case('sc')

					        <span>Single Choice</span>

					        @break

					    @case('r')

					        <span>Range</span>

					        @break



					    @default

					        <span>Content Page</span>

					@endswitch

                </td>

                <td>{{$qt->created_at}}</td>

                <td>

                	<a class="btn btn-primary" href="{{route('questionEdit',[$qt->id])}}"><i class="fa fa-edit"></i> Edit</a> &nbsp;

                	<a class="btn btn-danger" href="javascript:void(0);" data-url="{{route('deleteQuestion',[$qt->id])}}" onclick="deleteQuestion(this);"><i class="fa fa-trash"></i> Delete</a>

                </td>

            </tr>

            @endforeach

            @endif

        </tbody>

        <tfoot>

            <tr>

                <th>Id</th>

                <th>Title</th>

                <th>Type</th>

                <th>Created</th>

                <th>Action</th>

            </tr>

        </tfoot>

    </table>

		</div>

    </div>

@endsection

@section('myjsfile')

<script>

	$(document).ready(function() {

   		$('#myTable').DataTable();

	});

</script>

@endsection

