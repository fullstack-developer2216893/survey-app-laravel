
<ul class="nav navbar-nav side-nav">
	<li class="@if(Request::segment(1)=='home'){{'active'}}@else {{''}}@endif">
		<a href="{{route('home')}}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
	</li>
	<li class="@if(Request::segment(1)=='questions' || Request::segment(1)=='add-question' || Request::segment(1)=='edit-question'){{'active'}}@else {{''}}@endif">
		<a href="{{route('questionListing')}}"><i class="fa fa-question-circle"></i> Questions</a>
	</li>
	<li class="@if(Request::segment(1)=='surveys'|| Request::segment(1)=='add-survey' || Request::segment(1)=='edit-survey'){{'active'}}@else {{''}}@endif">
		<a href="{{route('surveyListing')}}"><i class="fa fa-tasks"></i> Survey</a>
	</li>
</ul>